require('dotenv/config');
const express = require('express');
const { MongoClient, ServerApiVersion } = require('mongodb');

const app = express();

const {
  APP_URL,
  APP_PORT,
  DB_DATABASE,
  DB_HOSTNAME,
  DB_PORT,
  DB_USERNAME,
  DB_PASSWORD
} = process.env;


app.get('/', function (req, res) {
  res.send(`Esta é uma página de teste que pode ser acessada pela porta: 3000 e 8080`);
});

MongoClient.connect(
  `mongodb://${DB_HOSTNAME}:${DB_PORT}/${DB_DATABASE}`,
  {
    useNewUrlParser: true,
    useUnifiedTopology: true
  })
  .then(result => {
    console.log('MongoDB Connect!!!');
  })
  .catch(error => {
    console.log(error);
  });


app.listen(APP_PORT);

console.log(`Funcionou! ${APP_URL}:${APP_PORT}`)
