
# Página Teste

Executa app.js em duas portas diferentes: 3000 e 8080.


## Rodando a Página Teste

Para inicializar, rode o seguinte comando na pasta raíz:

```bash
  docker-compose build && docker-compose up -d 
```


## Screenshots
Rodando
![Rodando](https://i.imgur.com/F3xa88v.png)

Finalizando
![Finalizando](https://i.imgur.com/Yltz43q.png)

Porta 3000
![Porta 3000](https://i.imgur.com/j50xIlt.png)

Porta 8080
![Porta 8080](https://i.imgur.com/gSZabI9.png)

<h1 align="center">Fim!</h1>
<p align="center">Feito com 💙 por kayrsant!</p>